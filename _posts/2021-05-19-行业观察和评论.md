---
layout: page
title: 行业观察和评论-循环经济的案例
categories:
     - 行业观察和评论
---
### 循环经济的必要性

万绿湖是华南地区第一大湖，水域面积370平方公里，蓄水量达139亿立方米。是广东、香港的重要饮用水源，也是广东省生态环境保护最好的区域之一。

近来[华南地区第一大湖万绿湖](https://www.163.com/dy/article/G9S59HBE0517P5DB.html)，也是号称[华南地区最大数据中心](https://www.aliyun.com/activity/daily/heyuanregion)所使用的电力及冷却水来源，出现了缺水情况，因旱季缺水，露出远古时期的花岗岩丘陵红色风化壳的残积物，如同“水上雅丹”。

这给也我们带来了重要的警示，自然资源与社会生活息息相关，增强健康与环保意识,打造绿色循环经济显得尤为重要。

循环经济是一个“资源—产品—再生资源”的反馈式流程从可持续发展角度，考虑资源对社会、生态、经济的作用力，使经济活动生态化。循环经济理论遵循“3R”原则，即减量化、再利用、再循环，从源头减少废弃物产生，以尽可能多的方式使用物品、将废弃物回收，运用到新产品生产上，对循环经济发展具有指导意义。

### 互联网领域中的循环经济
除了为可再生能源服务器供电外，互联网领域还能做些什么来应对气候变化，参与到循环经济当中？

[Tim Frick](https://www.mightybytes.com/blog/author/timfrick/)是美国Mightbytes首席执行官，[Mightbytes]()是位于芝加哥的数字代理商和共益企业,帮助有意识的公司，社会企业和大型非营利组织解决问题，扩大其影响力以及实现业务和营销目标。我们已经帮助数百个组织取得了实际的，可衡量的结果。Tim Frick相信企业可以用来解决社会和环境问题，互联网可以帮助实现这一目标。

[sustainable web design可持续网页设计](https://www.mightybytes.com/blog/sustainable-web-design/)致力于通过可持续的Web设计实践，可用于创建高性能，低碳的数字产品和服务从而减少互联网对环境的巨大影响。

[Eco-Grader](https://www.ecograder.com/)也是致力于可持续性网站设计，通过评分系统评估网站的可持续性分数，为网站的设计改进提供方向，例如通过建立从我们的页面修剪不需要的代码和请求的理念，我们可以进行更显着的性能改进。。

在使网站更具可持续性方面，最大的胜利可能是性能，用户体验和可持续性都巧妙地交织在一起。衡量数字产品可持续性的关键指标是能源使用，这包括服务器，客户端和在两者之间传输数据的中间通信网络所做的工作。Green hosting 是一种更绿色、更环保的网站托管方式。大多数绿色虚拟主机公司的目标是在尽最大可能限制碳排放量的同时提供高质量的服务。

早在2019，世界生态设计大会[WEDC](https://www.unido.org/news/eco-design-promotes-inclusive-sustainable-industrial-development)。Tim Frick分享了其创办网页设计共益企业、发起并创建W3C可持续网页设计社区小组的经验总结。主张网页设计公司要在互联网产品设计时考虑可持续，满足Profit、People、Planet三重底线。
